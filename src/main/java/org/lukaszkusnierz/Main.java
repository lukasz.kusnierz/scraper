package org.lukaszkusnierz;

import org.apache.commons.cli.*;

import java.util.Arrays;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		var cmd = commandLine(args);
		var fileNames = cmd.getOptionValues("F");
		var shouldCountChars = cmd.hasOption("C");
		var stopWords = new StopWords(cmd.getOptionValue("S").split(","));
		var counter = new Counter(stopWords);
		var printer = System.out;

		Arrays.stream(fileNames)
			.map(fileName -> counter.count(fileName, shouldCountChars))
			.peek(result -> {
				synchronized (counter) {
					result.print(printer);
				}
			})
			.map(SingleFileCounterResult::getResult)
			.parallel()
			.reduce(CounterResult.empty(), CounterResult::merge)
			.print(printer, "Total:");
	}

	private static Options options() {
		var options = new Options();
		Stream.of(
			Option.builder("F")
				.required()
				.desc("path to *.txt file (required)")
				.hasArg()
				.argName("fileName")
				.build(),
			Option.builder("S")
				.required()
				.desc("comma separated list of words to be ignored (required)")
				.hasArg()
				.argName("stopWords")
				.build(),
			Option.builder("C")
				.desc("count all characters in the file (optional)")
				.build()
		).forEach(options::addOption);
		return options;
	}

	private static CommandLine commandLine(String[] args) {
		var options = options();
		try {
			return new DefaultParser().parse(options, args);
		} catch (ParseException ex) {
			System.err.println(ex.getMessage());
			new HelpFormatter().printHelp("java -jar scraper.jar", options);
			System.exit(1);
			return null;
		}
	}
}
