package org.lukaszkusnierz;

import java.io.PrintStream;

class SingleFileCounterResult {

	private final String fileName;
	private final CounterResult result;

	SingleFileCounterResult(String fileName, CounterResult result) {
		this.fileName = fileName;
		this.result = result;
	}

	CounterResult getResult() {
		return result;
	}

	void print(PrintStream printer) {
		result.print(printer, "File: " + fileName);
	}
}
