package org.lukaszkusnierz;

import java.io.PrintStream;
import java.util.Optional;
import java.util.stream.Stream;

class CounterResult {

	private final long wordCount;
	private final Optional<Long> characterCount;

	static CounterResult empty() {
		return new CounterResult(0, Optional.empty());
	}

	CounterResult(long wordCount, Optional<Long> characterCount) {
		assert null != characterCount;
		this.wordCount = wordCount;
		this.characterCount = characterCount;
	}

	CounterResult merge(CounterResult other) {
		return new CounterResult(
			wordCount + other.wordCount,
			Stream.concat(characterCount.stream(), other.characterCount.stream()).reduce(Long::sum)
		);
	}

	void print(PrintStream printer, String title) {
		printer.println(title);
		printer.println("Words: " + wordCount);
		characterCount.ifPresent(count -> printer.println("Characters: " + count));
		printer.println();
	}
}
