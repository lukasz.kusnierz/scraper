package org.lukaszkusnierz;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class StopWords {

	private final Set<String> stopWords = new HashSet<>();

	StopWords(String[] array) {
		Arrays.stream(array)
			.map(this::normalize)
			.forEach(stopWords::add);
	}

	boolean contains(String word) {
		return stopWords.contains(normalize(word));
	}

	private String normalize(String token) {
		return token
			.toLowerCase()
			.replaceAll("[^a-z0-9]", "");
	}
}
