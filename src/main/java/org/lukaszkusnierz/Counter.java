package org.lukaszkusnierz;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.Scanner;

class Counter {

	// assuming UTF-8 encoding; charset can also be made an option, and/or we can add some charset guessing code
	private static final Charset CHARSET = StandardCharsets.UTF_8;

	private final StopWords stopWords;

	Counter(StopWords stopWords) {
		this.stopWords = stopWords;
	}

	SingleFileCounterResult count(String fileName, boolean shouldCountChars) {
		try {
			return new SingleFileCounterResult(
				fileName,
				new CounterResult(
					countWords(fileName),
					shouldCountChars ? Optional.of(countChars(fileName)) : Optional.empty()
				)
			);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	private long countWords(String file) throws IOException {
		try (var scanner = new Scanner(new BufferedReader(new FileReader(file, CHARSET)))) {
			return scanner.tokens()
				.filter(word -> !stopWords.contains(word))
				.count();
		}
	}

	private long countChars(String file) throws IOException {
		long count = 0;
		var buffer = new char[8 * 1024];
		int chunkSize;
		try (var reader = new FileReader(file, CHARSET)) {
			while (true) {
				chunkSize = reader.read(buffer);
				if (chunkSize <= 0) break;
				count += chunkSize;
			}
		}
		return count;
	}
}
